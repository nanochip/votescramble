# [TF2] Votescramble



## Description:
A team scrambling system that utilizes [NativeVotes](https://github.com/nosoop/sourcemod-nativevotes) and TF2's mp_scrambleteams command based on player voting.

## Dependencies
* **[NativeVotes](https://github.com/nosoop/sourcemod-nativevotes)**

## CVARS:
This plugin will auto generate a cvar config once the plugin is loaded in *tf/cfg/sourcemod/plugin.votescramble.cfg*
* **nano_votescramble_time [#.#]** - (Default: 15.0) Time in seconds the vote menu should last.
* **nano_votescramble_delay [#.#]** - (Default: 180.0) Time in seconds before players can initiate another team scramble vote.
* **nano_votescramble_chat_percentage [#.#]** - (Default: 0.20) How many players are required for the chat vote to pass? 0.20 = 20%.
* **nano_votescramble_menu_percentage [#.#]** - (Default: 0.60) How many players are required for the menu vote to pass? 0.60 = 60%.
* **nano_votescramble_minimum [#]** - (Default: 3) What are the minimum number of votes needed to initiate a chat vote?
* **nano_votescramble_canscrambletime [#.#]** - (Default: 60.0) The number of seconds after a round has officially started that a vote scramble can happen immediately (rather than waiting for next round). Use 0.0 to always happen immediately. Use -1.0 to only happen on next round.
* **nano_votescramble_gscramble [0/1]** - (Default: 0) If you're using the gScramble plugin and would like votescramble to initiate a gScramble instead of stock mp_scrambleteams, set this cvar to 1.

## Commands:
* **sm_votescramble** or chat say **votescramble** - (Default Access: Everyone) Vote to scramble the teams.
* **sm_vscramble** or chat say **vscramble** - (Default Access: Everyone) Vote to scramble the teams.
* **sm_forcescramble** - (Default Access: ADMFLAG_VOTE) Force a team scramble menu vote.

## Installation:
1. [Install Sourcemod](https://wiki.alliedmods.net/Installing_sourcemod).
2. Install [NativeVotes](https://github.com/nosoop/sourcemod-nativevotes) by extracting the addons/ folder to your tf/ folder.
3. Download this repository and extract the addons/ folder to your tf/ folder.
4. Restart the server or type "sm plugins load votescramble" in your server console.
5. Configure the plugin in *tf/cfg/sourcemod/plugin.votescramble.cfg* to your liking.

## To-Do List:
I am always welcome to suggestions!
* none

## Credits:
* **[Dr. McKay](https://forums.alliedmods.net/member.php?u=150845)** - I used his [Better Vote Scramble](https://forums.alliedmods.net/showthread.php?t=204746) plugin and simply added more configurations and NativeVotes support.
